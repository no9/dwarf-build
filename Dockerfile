FROM registry.access.redhat.com/ubi8/ubi as rhel8builder

# Example of installing development libs for the build
RUN yum install -y gcc openssl-devel && \
    rm -rf /var/cache/dnf && \
    curl https://sh.rustup.rs -sSf | sh -s -- -y

COPY . /app-build

WORKDIR "/app-build"
# Set up build paths and other config
ENV PATH=/root/.cargo/bin:${PATH}
RUN cargo build --release


FROM registry.access.redhat.com/ubi8/ubi-minimal

# ubi-minimal uses `microdnf` a small pkg management system.
RUN  microdnf update && microdnf install -y procps-ng

# Add a group and user call `appuser`
RUN addgroup -S appuser && adduser -S appuser -G appuser

WORKDIR "/app"
COPY --from=rhel8builder /app-build/target/release/stream-line-builds ./

# set the user that will run the application
USER appuser
